class LocationsController < ApplicationController
    def create
       @house = House.find(params[:house_id])
       @location = @house.locations.create(location_params)
       redirect_to @house
    end

    def destroy
        @house = House.find(params[:house_id])
        @location = @house.locations.find(params[:id])
        if @location.destroy
            flash[:success] = 'Object was successfully deleted.'
            redirect_to house_path(@house)
        else
            flash[:error] = 'Something went wrong'
            redirect_to house_path(@house)
        end
    end
    
    
    
    private
        def location_params
            params.require(:location).permit(:suburb, :address, :postcode, 
            :regionname, :propertycount, :distance)
        end
    
end
