class HousesController < ApplicationController
    def new
        @house = House.new
    end

    def edit
        @house = House.find(params[:id])
    end

    def create
        @house = House.new(house_params)
        if @house.save
          flash[:success] = "Object successfully created"
          redirect_to @house
        else
          flash[:error] = "Something went wrong"
          render 'new'
        end
    end
    
    def update
        @house = House.find(params[:id])
        if @house.update(house_params)
          flash[:success] = "Object was successfully updated"
          redirect_to @house
        else
          flash[:error] = "Something went wrong"
          render 'edit'
        end
    end

    def show
        @house = House.find(params[:id])
    end
    
    def destroy
        @house = House.find(params[:id])
        if @house.destroy
            flash[:success] = 'Object was successfully deleted.'
            redirect_to houses_path
        else
            flash[:error] = 'Something went wrong'
            redirect_to houses_path
        end
    end

    def index
        @houses = House.all
    end
    
    
    
    private
        def house_params
            params.require(:house).permit(:rooms, :house_type, :price, :method, :date)
        end
    
end
