class House < ApplicationRecord
    has_many :locations
    validates :house_type, presence: true
end
