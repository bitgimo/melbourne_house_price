Rails.application.routes.draw do
  get 'melbourne/home'
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
  resources :houses
  root 'melbourne#home'


  resources :houses do
    resources :locations
  end
end
