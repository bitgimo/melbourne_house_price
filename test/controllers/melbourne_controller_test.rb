require 'test_helper'

class MelbourneControllerTest < ActionDispatch::IntegrationTest
  test "should get home" do
    get melbourne_home_url
    assert_response :success
  end

end
