class CreateHouses < ActiveRecord::Migration[6.0]
  def change
    create_table :houses do |t|
      t.integer :rooms
      t.string :house_type
      t.float :price
      t.string :method
      t.date :date

      t.timestamps
    end

    create_table :locations do |t|
      t.string :suburb
      t.string :address
      t.string :postcode
      t.string :regionname
      t.integer :propertycount
      t.float :distance
      t.references :house, null: false, foreign_key: true

      t.timestamps
  end
end
